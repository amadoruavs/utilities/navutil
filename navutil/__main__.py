import asyncio

from navutil import drone

async def main():
    uav = drone.Drone("udp://localhost:14540")

    await uav.connect()
    
    async for telem in uav.get_telemetry():
        print(telem)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
