import asyncio
import mavsdk
from aiostream import stream

from interoperator.telemetry import Telemetry


class Drone(mavsdk.System):
    def __init__(self, address, *args, **kwargs):
        """Create a new drone connection
        :param address the address of the drone's MAVLink server
        """

        mavsdk.System.__init__(self, *args, **kwargs)
        self.address = address
        self.uuid = -1

    async def connect(self):
        await super().connect(system_address=self.address)

        async for state in self.core.connection_state():
            if state.is_connected:
                self.uuid = state.uuid
                break

    async def acquire_gps_lock(self):
        async for health in self.telemetry.health():
            if health.is_global_position_ok:
                break

    async def get_telemetry(self):
        """Returns interop telemetry using the interoperator
        dataclass, which can be directly posted.
        This includes latitude, longitude, altitude,
        and orientation."""
        
        for package in stream.zip(self.telemetry.position(), self.telemetry.attitude_euler()):
            yield package
        # async for position in self.telemetry.position():
            # async for attitude in self.telemetry.attitude_euler():
                # yield Telemetry(latitude=position.latitude_deg,
                        # longitude=position.longitude_deg,
                        # altitude=position.relative_altitude_m * 3.28084,
                        # heading=(attitude.yaw_deg + 360) % 360)
