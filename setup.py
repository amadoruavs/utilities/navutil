import setuptools

with open("README.md") as fh:
    long_description = fh.read()

setuptools.setup(
        name="navutil",
        version="0.0.1",
        author="AmadorUAVs",
        author_email="officers@amadoruavs.com",
        description="Navigation & MAVSDK utilities",
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="https://gitlab.com/amadoruavs/interoperator",
        packages=setuptools.find_packages(),
        classifiers=[
            "Programming Language :: Python :: 3",
            ],
        python_requires=">=3.6",
)
